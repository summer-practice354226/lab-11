﻿#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

using namespace std;

void readMatrixFromFile(int**& matrix, int& numRows, int& numCols, const string& filename) {
    ifstream file(filename);
    if (!file.is_open()) {
        cout << "Failed to open file: " << filename << endl;
        return;
    }

    file >> numRows >> numCols;

    matrix = new int* [numRows];
    for (int i = 0; i < numRows; i++) {
        matrix[i] = new int[numCols];
        for (int j = 0; j < numCols; j++) {
            file >> matrix[i][j];
        }
    }

    file.close();
}

int* calculateColumnCharacteristics(int** matrix, int numRows, int numCols) {
    int* columnCharacteristics = new int[numCols];
    for (int j = 0; j < numCols; j++) {
        columnCharacteristics[j] = 0;
        for (int i = 0; i < numRows; i++) {
            columnCharacteristics[j] += abs(matrix[i][j]);
        }
    }
    return columnCharacteristics;
}

void sortColumnsByCharacteristics(int**& matrix, int numRows, int numCols, int* columnCharacteristics) {
    int** sortedMatrix = new int* [numRows];
    for (int i = 0; i < numRows; i++) {
        sortedMatrix[i] = new int[numCols];
    }

    pair<int, int>* indexCharPairs = new pair<int, int>[numCols];
    for (int i = 0; i < numCols; i++) {
        indexCharPairs[i] = { columnCharacteristics[i], i };
    }

    sort(indexCharPairs, indexCharPairs + numCols,
        [](const pair<int, int>& a, const pair<int, int>& b) {
            return a.first < b.first;
        });

    for (int i = 0; i < numCols; i++) {
        int newIndex = indexCharPairs[i].second;
        for (int j = 0; j < numRows; j++) {
            sortedMatrix[j][i] = matrix[j][newIndex];
        }
    }

    for (int i = 0; i < numRows; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = sortedMatrix;

    delete[] indexCharPairs;
}

int main() {
    int** matrix;
    int numRows, numCols;
    readMatrixFromFile(matrix, numRows, numCols, "input.txt");

    int* columnCharacteristics = calculateColumnCharacteristics(matrix, numRows, numCols);

    cout << "Original matrix:" << endl;
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    cout << "\nColumn characteristics:" << endl;
    for (int i = 0; i < numCols; i++) {
        cout << columnCharacteristics[i] << " ";
    }
    cout << endl;

    sortColumnsByCharacteristics(matrix, numRows, numCols, columnCharacteristics);

    cout << "\nSorted matrix:" << endl;
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    for (int i = 0; i < numRows; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    delete[] columnCharacteristics;

    return 0;
}